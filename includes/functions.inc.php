<?php

/* Don't start if already started */
if (!isset($_SESSION))
  session_start();

/*
 * ------------- Sign-up stuff! -------------
 */

/**
 * Check if any of the input fields is empty.
 *
 * @param string $username
 * @param string $password
 * @param string $passwordR
 *
 * @return bool
 */
function emptyInputSignup(string $username, string $password, string $passwordR) {
  return (empty($username) || empty($password) || empty($passwordR)) ? true :
  false;
}


/**
 * Check if passwords match
 *
 * @param string $password
 * @param string $passwordR
 *
 * @return bool
 */
function pwdNotMatch(string $password, string $passwordR) {
  return ($password !== $passwordR) ? true : false;
}


/**
 * Create the user
 *
 * @param PDO $db
 * @param string $username
 * @param string $password
 */
function createUser(PDO $db, string $username, string $password) {
  $hashedPwd = password_hash($password, PASSWORD_DEFAULT);
  $stmt = $db->prepare("INSERT INTO users (userName, userPwd, timeCreated)
    VALUES (?, ?, ?);");

  if (!$stmt) {
    $_SESSION["error"] = "stmtfailed";
    header("Location: ../signup.php");
    exit;
  }

  $timeCreated = time();
  $stmt->execute([$username, $hashedPwd, $timeCreated]);
  header("Location: ../login.php");
}

/*
 * ------------- Login stuff! -------------
 */

/**
 * Check if the input fields are empty
 *
 * @param string $username
 * @param string $password
 *
 * @return bool
 */
function emptyInputLogin(string $username, string $password) {
  return (empty($username) || empty($password)) ? true : false;
}


/**
 * Check if user credentials are the same as those in the database
 *
 * @param PDO $db
 * @param string $username
 * @param string $password
 */
function loginUser(PDO $db, string $username, string $password) {
  $userExists = userExists($db, $username);

  if ($userExists === false) {
    $_SESSION["error"] = "wronglogin";
    header("Location: ../login.php");
    exit;
  }

  $hashedPass = $userExists["userPwd"];
  $checkPass = password_verify($password, $hashedPass);

  if ($checkPass === false) {
    $_SESSION["error"] = "wronglogin";
    header("Location: ../login.php");
  } else {
    $_SESSION["userid"] = $userExists["userID"];
    $_SESSION["username"] = $userExists["userName"];
    $_SESSION["timecreated"] = $userExists["timeCreated"];
    header("Location: ../index.php");
  }
}


/*
 * ------------ Common stuff -----------
 */

/**
 * Check if the username contains any characters that aren't characters and
 * numbers
 *
 * @param string $username
 *
 * @return bool
 */
function invalidUsername(string $username) {
  return preg_match("/^[a-zA-Z0-9]*$/", $username) ? false : true;
}


/**
 * Check if the user exists
 *
 * @param PDO $db
 * @param string $username
 *
 * @return array|bool
 */
function userExists(PDO $db, string $username) {
  $stmt = $db->prepare("SELECT * FROM users WHERE userName = ?;");

  if (!$stmt) {
    $_SESSION["error"] = "stmtfailed";
    header("Location: ../signup.php");
    exit;
  }

  $stmt->execute([$username]);
  /* False is returned on failure or if there are no more rows */
  return $stmt->fetch(PDO::FETCH_ASSOC);
}


/*
 * ------------- Chat-room stuff ------------
 */

/**
 * Echo out the sanitized message, along with who sent it and when.
 *
 * @param array $row
 */
function echoMsg(array $row) {
  $messageID = $row["messageID"];
  $userID = $row["userID"];
  $userName = $row["userName"];
  $msgContent = $row["msgContent"];
  $timeSent = date("Y-m-d@H:i:s", $row["timeSent"]);

  echo "<p class='msgtime'>".$timeSent."</p>";
  echo "<p class='msg'>".$userName.": ".$msgContent."</p>";
}


/**
 * Get messages from the database and append to the array in the session
 *
 * @param PDO $db
 */
function getUnreadMessages(PDO $db) {
  /* Ideally message data would be cached for performance reasons */

  /* Getting new database entries */
  if (isset($_SESSION["lastmsgid"])) {
    $lastMsgID = $_SESSION["lastmsgid"];
    $stmt = $db->prepare("SELECT * FROM messages WHERE messageID > ?;");

    if (!$stmt) {
      $_SESSION["error"] = "stmtfailed";
      header("Location: ../chat.php");
      exit;
    }

    $stmt->execute([$lastMsgID]);
    $rows = array_reverse($stmt->fetchAll(PDO::FETCH_ASSOC));

    if (isset($rows[0])) {
      $_SESSION["rows"] = array_merge_recursive($rows, $_SESSION["rows"]);
      $_SESSION["lastmsgid"] = $_SESSION["rows"][0]["messageID"];
    }
  /* Getting everything from the database */
  } else {
    $stmt = $db->prepare("SELECT * FROM messages;");

    if (!$stmt) {
      $_SESSION["error"] = "stmtfailed";
      header("Location: ../chat.php");
      exit;
    }

    $stmt->execute();
    $rows = array_reverse($stmt->fetchAll(PDO::FETCH_ASSOC));
    if (isset($rows[0])) {
      $_SESSION["lastmsgid"] = $rows[0]["messageID"];
      $_SESSION["rows"] = $rows;
    }
  }

  if (isset($_SESSION["rows"]))
    foreach ($_SESSION["rows"] as $row)
      echoMsg($row);
}


/**
 * Send the message after we sanitize it, and record it to the database.
 *
 * @param PDO $db
 *
 * @return void
 */

function sendMsg(PDO $db) {
  /* Convert HTML to normal characters so that it does not get activated  */
  $message = htmlspecialchars($_POST["message"], ENT_QUOTES);

  /* Make sure there is a message to be sent, client-side handles the mssage */
  if (empty(str_replace(' ', '', $message)))
    return;

  $userID = $_SESSION["userid"];
  $userName = $_SESSION["username"];
  $timeSent = time();

  $stmt = $db->prepare("INSERT INTO messages (userID, userName, msgContent, timeSent) VALUES
  (?, ?, ?, ?);");

  if (!$stmt) {
    $_SESSION["error"] = "stmtfailed";
    header("Location: ../chat.php");
    exit;
  }
  $stmt->execute([$userID, $userName, $message, $timeSent]);
}
