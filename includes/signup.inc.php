<?php

if (isset($_POST["register"])) {
  require_once 'dbh.inc.php';
  require_once 'functions.inc.php';

  /**
   * @var string
   */
  $username = $_POST["username"];

  /**
   * @var string
   */
  $password = $_POST["pwd"];

  /**
   * @var string
   */
  $passwordR = $_POST["pwdrepeat"];

  if (emptyInputSignup($username, $password, $passwordR))
    $_SESSION["error"] = "emptyinput";
  else if (invalidUsername($username))
    $_SESSION["error"] = "invalidusername";
  else if (pwdNotMatch($password, $passwordR))
    $_SESSION["error"] = "pwdnotmatch";
  else if (userExists($db, $username) !== false)
    $_SESSION["error"] = "usernametaken";
  else {
    createUser($db, $username, $password);
    exit(); /* Exit because the browser has already been redirected. */
  }
}
header("Location: ../signup.php");
