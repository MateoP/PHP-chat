<?php

if (isset($_POST["login"])) {
  /**
   * @var string
   */
  $username = $_POST["username"];

  /**
   * @var string
   */
  $password = $_POST["password"];

  require_once 'dbh.inc.php';
  require_once 'functions.inc.php';

  if (emptyInputLogin($username, $password))
    $_SESSION["error"] = "emptyinput";
  else if (invalidUsername($username))
    $_SESSION["error"] = "invalidusername";
  else {
    loginUser($db, $username, $password);
    exit; /* Exit because the browser has already been redirected. */
  }
}
header("Location: ../login.php");
