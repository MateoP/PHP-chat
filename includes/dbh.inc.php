<?php

/**
 * Log the message to server console and server system logs.
 *
 * @param bool $error
 * @param string $message
 */
function report($error, $message) {
  error_log($message);
  syslog($error ? LOG_ERR : LOG_CRIT, $message);
}


/* Try to access the database, if it does not exist - create it. */
try {
  $db= new PDO('sqlite:'.__DIR__.'/loginsystem.sq3');

  /* Set mode to exceptions */
  $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

  /* Test if the database exists, if not - we'll handle the error */
  $stmt = $db->prepare("SELECT * FROM users;");
  $stmt->execute();
} catch (PDOException $e) {
  $error = $e->getCode();

  /* The database is not created yet, so create it. */
  if ($error === 'HY000') {
    report(false, "There is no database, creating one right now!");
    $sql = "
      CREATE TABLE users(
        userID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
        userName varchar(128) NOT NULL,
        userPwd varchar(128) NOT NULL,
        timeCreated INTEGER NOT NULL
        );
    ";
    $stmt = $db->prepare($sql);
    $stmt->execute();

    $sql = "
      CREATE TABLE messages(
        messageID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
        userID INTEGER NOT NULL,
        userName varchar(128) NOT NULL,
        msgContent TEXT NOT NULL,
        timeSent INTEGER NOT NULL
        );
    ";
    $stmt = $db->prepare($sql);
    $stmt->execute();

    report(false, "New database created!");
    report(false, "Please move the new database to a safer place, and change
      the path to it in includes/dbh.inc.php!");
  } else {
    /* Trigger an error on server-side, spit out the message and error, and
    log it to the system, in case something freezes or crashes? */
    trigger_error("Unable to connect to the database! - " . $e->getMessage(), E_USER_ERROR);
    report(true, "Unable to connect to the database! - " . $e->getMessage());
  }
}
