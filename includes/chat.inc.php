<?php

require_once 'dbh.inc.php';
require_once 'functions.inc.php';

if (!isset($_SESSION["userid"]))
  $_SESSION["error"] = "nologin";
else if (isset($_POST["send"])) {
  if (isset($_POST["message"])) {
    if (strlen($_POST["message"]) > 2048)
      $_SESSION["error"] = "toolong";
    else
      sendMsg($db);
  } else
    $_SESSION["error"] = "nomessage";
}
header("Location: ../chat.php");
