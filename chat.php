<?php
include_once 'header.php';
require_once 'includes/dbh.inc.php';
require_once 'includes/functions.inc.php';
?>

    <div class="space-fill">
      <form id="chat-form" action="includes/chat.inc.php" method="post">
        <textarea id="input" autofocus required name="message" placeholder="Type your message here"></textarea>
        <input id="button" type="submit" name="send" value="Send">
      </form>
    </div>

<?php
    if (isset($_SESSION["error"])) {
      switch($_SESSION["error"]){
      case "nologin":
        unset($_SESSION["error"]);
        echo "<p class='small text-danger'>Log-in in order to chat!</p>";
        break;
      case "nomessage":
        unset($_SESSION["error"]);
        echo "<p class='small text-danger'>Provide a message.</p>";
        break;
      case "toolong":
        unset($_SESSION["error"]);
        echo "<p class='small text-danger'>Maximum message size is 2048
          characters!</p>";
        break;
      case "stmtfailed":
        unset($_SESSION["error"]);
        echo "<p class='small text-danger'>Something went wrong, please try
          again!</p>";
        break;
      default:
        unset($_SESSION["error"]);
      }
    }

    echo "<div id='chat'>";
      getUnreadMessages($db);
    echo "</div>";
?>

    <script src="chat.js"></script>

<?php include_once 'footer.php'; ?>
