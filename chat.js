const inputForm= document.querySelector('#chat-form');
const msgInput = document.querySelector('#chat-form #input');
const sendBtn = document.querySelector('#chat-form #button');

inputForm.addEventListener('submit', resetText);
/* Restore the message that was was being typed, after a refresh */
msgInput.value = localStorage.getItem('message');

fetchMessages();
/* Reload after a while, so that we receive new messages */
setTimeout(reloadPage, 10000);


function fetchMessages() {
  setTimeout(fetchMessages, 100);
  /* Store the message still being typed */
  localStorage.setItem('message', msgInput.value);
}


function resetText(e) {
  /* If we set the input value directly - the sent message will be empty */
  localStorage.setItem('message', '');
}


function reloadPage() {
  history.go(0);
}
