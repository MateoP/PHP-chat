<?php session_start(); ?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" content="width=device-width, initial-scale=1.0">
    <title>Web-site for learning PHP, SQL and best practices.</title>
    <link rel="stylesheet" href="style.css">
  </head>

  <body>
    <header id="main-header">
      <h1>My little website</h1>
    </header>

    <div id="nav-container">
      <ul id="nav-bar">
        <li><a href="index.php">Home</a></li>
        <?php
          if (isset($_SESSION["username"])) {
            echo "<li><a href='profile.php'>Profile</a>  ({$_SESSION['username']})</li>";
            echo "<li><a href='chat.php'>Chat</a></li>";
            echo "<li><a href='includes/logout.inc.php'>Logout</a></li>";
          } else {
            echo "<li><a href='signup.php'>Sign Up</a></li>";
            echo "<li><a href='login.php'>Log in</a></li>";
          }
        ?>
      </ul>
    </div>
