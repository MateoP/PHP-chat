<?php include_once "header.php" ?>

    <section class="acc-section">
      <h2>Sign-up.</h2>
      <form id="register-form" action="includes/signup.inc.php" method="post">
        <div class="login-group">
          <label>Username: </label>
          <input autofocus type="text" name="username" placeholder="JohnSmith">
        </div>
        <div class="login-group">
          <label>Password: </label>
          <input type="password" name="pwd" placeholder="*****************">
        </div>
        <div class="login-group">
          <label>Repeat password: </label>
          <input type="password" name="pwdrepeat" placeholder="*****************">
        </div>
        <button class="button" type="submit" name="register">Sign Up</button>
      </form>

      <div class="error">
        <?php
          if (isset($_SESSION["error"])) {
            switch ($_SESSION["error"]) {
            case "emptyinput":
              unset($_SESSION["error"]);
              echo "<p class='small text-danger'>Fill in all the fields!</p>"; break;
            case "invalidusername":
              unset($_SESSION["error"]);
              echo "<p class='small text-danger'>Choose a proper username!</p>"; break;
            case "pwdnotmatch":
              unset($_SESSION["error"]);
              echo "<p class='small text-danger'>Passwords do not match!</p>"; break;
            case "usernametaken":
              unset($_SESSION["error"]);
              echo "<p class='small text-danger'>Username already taken!</p>"; break;
            case "stmtfailed":
              unset($_SESSION["error"]);
              echo "<p class='small text-danger'>Something went wrong, please try
                again!</p>"; break;
            default:
              unset($_SESSION["error"]);
            }
          }
        ?>
      </div>
    </section>

<?php include_once "footer.php" ?>
