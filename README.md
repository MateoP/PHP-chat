Little website that has: a chat and a login system.
It is written in PHP, CSS and a little bit of JavaScript, which is 100%
optional.

Database used here is SQLite3, although I did utilize a PDO, so it should be
somewhat universal.
This means that you need the sqlite3 and pdo-sqlite PHP extension if you do not have it
installed already.

![homepage](images/homepage.png)
![register](images/sign-up.png)
![login](images/login.png)
![profile](images/profile.png)
![chat](images/chat.png)

WARNING: the database should NOT be in this repository! Please move it and
update the path to it in the code, for security reasons. (in file
includes/dbh.inc.php)

Licensed under: Zero-Clause BSD
