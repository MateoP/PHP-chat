<?php include_once "header.php" ?>

    <section class="acc-section">
      <h2>Log in.</h2>
      <form id="login-form" action="includes/login.inc.php" method="post">
        <div class="login-group">
          <label>Username: </label>
          <input autofocus type="text" name="username" placeholder="JohnSmith">
        </div>
        <div class="login-group">
          <label>Password: </label>
          <input type="password" name="password" placeholder="*****************">
        </div>
        <input class="button" type="submit" name="login" value="Log in">
      </form>

      <div class="error">
        <?php
          if (isset($_SESSION["error"])) {
            switch ($_SESSION["error"]) {
            case "emptyinput":
              unset($_SESSION["error"]);
              echo "<p class='small text-danger'>Fill in all the fields!</p>"; break;
            case "invalidusername":
              unset($_SESSION["error"]);
              echo "<p class='small text-danger'>Insert a proper username!</p>"; break;
            case "wronglogin":
              unset($_SESSION["error"]);
              echo "<p class='small text-danger'>Incorrect credentials!</p>"; break;
            default:
              unset($_SESSION["error"]);
            }
          }
        ?>
      </div>
    </section>

<?php include_once "footer.php" ?>
