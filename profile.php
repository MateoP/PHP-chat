<?php include_once 'header.php'; ?>

    <div class="space-fill">
      <h2>Your profile information</h2>
    </div>
    <div id="profile">
      <?php
        if (isset($_SESSION["userid"])) {
          $username = $_SESSION["username"];
          $timeCreated = date("Y-m-d@H:i:s", $_SESSION["timecreated"]);
          echo "Your username is: <b>".$username."</b><br>";
          echo "Your account has been created on: <b>".$timeCreated." UTC</b>";
        }
       ?>
    </div>

<?php include_once 'footer.php'; ?>
